#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


#Implement the logic of the activation function (=> Step Function!) 
#@Return: 0 or 1
def activation(value):
    if value >= 0:
        return 1
    return 0
    
#Implement the code needed for the perceptron. Use np.dot(...)
#@Args: 
#  x...value (vector => 1D Tensor), 
#  w...weight (vector => 1D Tensor), 
#  b...bias (just a value)
#@Return: Value (Skalar eg. 1.0)
def perceptron(x, w, b):
    return np.dot(x, w) + b


# In[3]:


# Test your code and check the results!
ex1 = np.array([0, 0])
ex2 = np.array([0, 1])
ex3 = np.array([1, 0])
ex4 = np.array([1, 1])

def OR_perceptron(x):
    # Define weights 
    w = [1,1]
    # Define Bias-value
    b = -1
    #Calculate Sigma
    sig = perceptron(x, w, b)
    return activation(sig)
    #Call Acivation Function

print(f"OR {ex1[0]},{ex1[1]}:", OR_perceptron(ex1))
print(f"OR {ex2[0]},{ex2[1]}:", OR_perceptron(ex2))
print(f"OR {ex3[0]},{ex3[1]}:", OR_perceptron(ex3))
print(f"OR {ex4[0]},{ex4[1]}:", OR_perceptron(ex4))


# In[ ]:




