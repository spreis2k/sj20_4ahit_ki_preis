#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from sklearn.metrics.pairwise import cosine_similarity


# In[2]:


a = np.array([4,0,5,3,5,0,0])
b = np.array([0,4,0,4,0,5,0])
c = np.array([2,0,2,0,1,0,0])
d = np.array([0,5,0,3,0,5,4])

table = np.array([[4,0,5,3,5,0,0],[0,4,0,4,0,5,0],[2,0,2,0,1,0,0],[0,5,0,3,0,5,4]])

a_recenter = np.array([-0.25,0,0.75,-1.25,0.75,0,0])
b_recenter = np.array([0,-0.33,0,-0.33,0,0.67,0])
c_recenter = np.array([0.33,0,0.33,0,-0.67,0,0])
d_recenter = np.array([0,0.75,0,-1.25,0.75,-0.25])


# #### Task #1

# In[3]:


cosine_similarity(a.reshape(1,-1), b.reshape(1,-1))


# In[4]:


cosine_similarity(a.reshape(1,-1), c.reshape(1,-1))


# #### Task #2

# In[5]:


cosine_similarity(a_recenter.reshape(1,-1), b_recenter.reshape(1,-1))


# In[6]:


cosine_similarity(a_recenter.reshape(1,-1), c_recenter.reshape(1,-1))


# #### Task #3

# In[16]:


def recenter(matrix):
    mean = []
    matrix_redone = []
    line = []
    
    nn_counter = 0 # not null counter
    line_counter = 0
    
    for i in matrix:
        mean_l = 0
        nn_counter = 0 
        for j in i:
            mean_l += j
            if(j != 0):
                nn_counter += 1
        mean_l = mean_l / nn_counter
        mean.append(mean_l)

    for i in matrix:
        line = []
        for j in i:
            if(j != 0):
                line.append(j-mean[line_counter])
            else:
                line.append(0)
        line_counter+=1
        matrix_redone.append(line)
            
    return(matrix_redone)


# In[17]:


def checksum_check(matrix):
    checksums_saved = []
    checksum = 0
    
    sum_per_line_zero = True
    
    for i in matrix:
        checksum = 0
        for j in i:
            checksum += j
        checksums_saved.append(round(checksum))
        
    for i in checksums_saved:
        if(i != 0):
            sum_per_line_zero = False
            
    return sum_per_line_zero


# In[13]:


print(recenter(table))
print(checksum(recenter(table)))


# In[ ]:




