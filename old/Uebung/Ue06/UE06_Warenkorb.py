#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import csv
import copy
# Constants for the exercises:
#WORKING_DIR = os.getcwd()
#DATA_DIR = os.path.join(os.path.dirname(WORKING_DIR), 'data')

file = []

sum_original = 0
sum_modified = 0

def read_and_save_to_array_csv(filepath):
    with open(filepath) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")  
        
        global file
        
        for line in csv_reader:
            file.append(line)
            
        for r in range(0,len(file)):
            for c in range(1, 3):
                file[r][c] = float(file[r][c])
                   
        
def add_column_and_format_in_csv(output_file):
        with open(output_file, 'w', newline='') as write_obj:
            csv_writer = csv.writer(write_obj)
            
            global file
            
            for line in file:
                line.insert(1,line[1]*line[2])
                line.pop(2)
                line.pop(2)
                csv_writer.writerow(line)
                
def assert_original():
    global sum_original
    
    for line in file:
        sum_original += line[1]*line[2]
    
def assert_modified():
    global sum_modified
    
    for line in file:
        sum_modified += line[1]
        
def assert_showdown():
    assert sum_original == sum_modified
        
    
            
            
read_and_save_to_array_csv("shopping_cart.txt")
assert_original()
add_column_and_format_in_csv("shopping_cart2.txt")
assert_modified()
assert_showdown()


# In[ ]:





# In[ ]:




