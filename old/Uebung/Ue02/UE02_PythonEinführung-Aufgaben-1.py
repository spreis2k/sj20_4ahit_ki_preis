#!/usr/bin/env python
# coding: utf-8

# # UE02 Python Einführung - Aufgaben 
# Arbeiten Sie nachfolgenden Aufgabenstellungen durch. 
# 
# >**Hinweis:** Abzugeben ist ein Python-File (Export siehe Menüeintrag `File > Download as`).

# ## Task 2.1.1 - Strings
# Beantworten Sie nachfolgende Frage bzw. schreiben Sie jenen Code, den es zur Erfüllung der jeweiligen Aufgabenstellung braucht:
# 1. Macht es einen Unterschied, ob man einfache oder doppelte Anführungsstriche verwendet? 
# 2. Erzeugen Sie mit <code>print(...)</code> folgende Ausgabe : **"Now, I'm able to use single quotes!"**
# 3. Geben Sie durch Verwendung von <code>x</code> und <code>y</code> die Zeichenkette **Hello, world!** mit <code>print(...)</code> aus.

# Antwort zu 1.: Nein. bei python ist sind beide das gleiche

# In[9]:


print("Now, I am able to use single quotes!");


# In[11]:


#3.
hello, world = "Hello", "World"
print(hello +", " + world +"!")


# ## Task 2.1.2 - Indexbasierte String-Manipulation
# Nehmen Sie vorher https://docs.python.org/3.8/tutorial/introduction.html#strings durch, und zwar ab der Textstelle "*Strings can be indexed...*" Als Bearbeitungsgrundlage dient <code>show</code>:
# 1. Geben Sie den zweiten Buchstaben der Zeichenkette aus
# 2. Geben Sie das Wort **eating** aus
# 3. Geben Sie alles nach **Software** aus
# 4. Geben Sie alles vor **the** aus
# 5. Geben Sie die letzten 3 Buchstaben der Zeichenkette aus

# In[24]:


show = "Software is eating the world!"

print(show[1])
print(show[12:18])
print(show[8:])
print(show[0:15])
print(show[len(show)-3:len(show)])


# ## Task 2.1.3 - if, elif, else Statements
# 
# Es gilt:
# - Wenn <code>x</code> 'true' ist, hat die Ausgabe '**x was True!**' zu erfolgen
# - sonst **'x was False!'**

# In[36]:


x = False
if (x==True):
    print("x was True!")
else:
    print("x was False!")


# ## Task 2.2.4 - if, elif, else Statements
# 
# Es gilt:
# - Wenn <code>person</code> 'Georg' ist, hat die Ausgabe '**Welcome Georg**' zu erfolgen.
# - Ist <code>person</code> 'Jimmy', dann '**Welcome Jimmy**'.
# - Sonst '**Welcome, what is your name?**'

# In[41]:


person = 'Georg'
if(person =="Georg"):
    print ("Welcome Georg")
elif (person == "Jimmy"):
    print("Welcome Jimmy")
else:
    print("Welcome, what is your name?")


# ## Task 2.2.5 - Loops, Lists, Dicts etc. 

# Iterieren Sie über die Liste <code>list1</code> und fügen Sie hierbei alle geraden Einträge der neu zu erstellenden Liste <code>even</code> und alle ungeraden der Liste <code>odd</code> hinzu. Setzen Sie den Modulo-Operator ein.

# In[73]:


list1 = [1,2,3,4,5,6,7,8,9,10]
oddlist = []
evenlist = []
for i in list1: 
    if (i % 2 == 0): 
         evenlist.append(i) 
    else: 
         oddlist.append(i) 
print(oddlist)
print(evenlist)


# ## Task 2.2.6 - Loops, Lists, Dicts etc. 
# Geben Sie die *Keys* als auch die *Values* des Dictionaries <code>d1</code> aus.

# In[75]:


d1 = {'k1':1,'k2':2,'k3':3}

for item in d1:
    print("Key : {} , Value : {}".format(item,d1[item]))


# ## Task 2.2.7 - Loops, Lists, Dicts etc. 
# Erstellen ein Dictionary mit ein paar Namenseinträgen. Erstellen Sie in Folge ein zweites Dictionary mit weiteren Namenseinträgen. Fügen Sie beide Dictionaries zusammen und stellen Sie hierbei sicher, dass im "neuen" Dictionary keine Duplikate vorhanden sind. (Anm.: Testszenario mit mind. einer Namensgleichheit. Die Überprüfung hat manuell zu erfolgen).

# In[25]:


n1, n2 = {"Anton":5,"Alex":2,"Andreas":3,"Igor":4},{"Wazslav":1,"Toni":2,"Dominique":3,"Igor":4,"Anton":5,"Andi":6} 

shared_items = {shared: n1[shared] for shared in n1
                if shared in n2 and n1[shared] == n2[shared]}
print(shared_items)
      
      
n1.update(n2)
print(n1)


# ## Task 2.2.8
# Erstellen Sie mithilfe der *Built-in Function* `range` eine Liste mit Werten, die folgende Werte enthält: `[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]`. Details bzgl. Verwendung von `range` liefert diese [Quelle](https://docs.python.org/3.8/library/functions.html).

# In[46]:


list1 = list(range(0,101,10))
print(list1)


# In[ ]:





# In[ ]:




