#!/usr/bin/env python
# coding: utf-8

# In[1]:


import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow.keras 
from tensorflow.keras.datasets import boston_housing
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.utils import to_categorical


# In[2]:


(train_data, train_labels), (test_data, test_labels) = tf.keras.datasets.boston_housing.load_data(
    path="boston_housing.npz", test_split=0.2, seed=113
)


# In[3]:


train_data.ndim


# In[4]:


print(train_data.shape)
print(train_labels.shape)
print(test_data.shape)
print(test_labels.shape)


# In[5]:


train_data


# In[6]:


train_data_df = pd.DataFrame(train_data)
test_data_df = pd.DataFrame(test_data)


# In[7]:


train_data_df.info()


# In[8]:


train_data_df.mean()


# In[9]:


train_data_df.describe()


# In[10]:


model = Sequential()
model.add(Dense(64, activation="relu", input_shape=(train_data.shape[1],)))
model.add(Dense(64, activation="relu"))
model.add(Dense(1))
model.compile(optimizer="rmsprop", loss="mse", metrics=["mae"]) 

model.summary()


# In[11]:


plt.boxplot(train_data)
plt.title("nicht normalisiert")
plt.show()


# In[12]:


model_history = model.fit(train_data, train_labels, epochs=80, batch_size=4, verbose=0)
type(model_history.history)


# In[13]:


model.evaluate(test_data, test_labels) 


# In[14]:


mean_train_data = train_data_df - train_data_df.mean()
mean_test_data = test_data_df - test_data_df.mean()


# In[15]:


train_data_std = mean_train_data / mean_train_data.std()
test_data_std = mean_test_data / mean_test_data.std()


# In[19]:


plt.boxplot(train_data_std.to_numpy())
plt.title("normalisiert")
plt.show()


# In[17]:


model.fit(train_data_std, train_labels, epochs=80, batch_size=4, verbose=0)


# In[18]:


model.evaluate(test_data_std, test_labels) 

