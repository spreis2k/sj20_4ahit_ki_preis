#!/usr/bin/env python
# coding: utf-8

# In[3]:


import matplotlib.pyplot as plt
import numpy as np


# In[5]:


plt.plot([1,2,3,4],[1,4,9,16])
plt.title("First Plot")
plt.xlabel("X label")
plt.ylabel("Y label")
plt.show()


# In[6]:


plt.figure(figsize=(15,5))
plt.plot([1,2,3,4],[1,4,9,16])
plt.show()


# In[7]:


plt.plot([1,2,3,4],[1,4,9,16],"go")
plt.title("First Plot")
plt.xlabel("X label")
plt.ylabel("Y label")
plt.show()


# In[8]:


x = np.arange(1,5)
y = x**3
plt.plot([1,2,3,4],[1,4,9,16],"go", x, y,"r^")
plt.title("First Plot")
plt.xlabel("X label")
plt.ylabel("Y label")
plt.show()


# In[10]:


plt.subplot(1,2,1)
plt.plot([1,2,3,4],[1,4,9,16],"go")
plt.title("1st subplot")

plt.subplot(1,2,2)
plt.plot(x,y,"r^")
plt.title("2nd subplot")

plt.suptitle("My sub-plots")
plt.show()


# In[11]:


x = np.arange(1,5)
y = x**3
fig, ax = plt.subplots(nrows=2,ncols=2,figsize=(6,6))
ax[0,1].plot([1,2,3,4],[1,4,9,16],"go")
ax[1,0].plot(x,y,"r^")
ax[0,1].set_title("Squares")
ax[1,0].set_title("Cubes")
plt.show()


# In[12]:


divisions = ["Div-A", "Div-B", "Div-C", "Div-D", "Div-E"]
division_average_marks = [70,82,73,65,68]

plt.bar(divisions, division_average_marks, color="green")
plt.title("Bar Graph")
plt.xlabel("Divisions")
plt.ylabel("Marks")
plt.show()


# In[13]:


divisions = ["Div-A", "Div-B", "Div-C", "Div-D", "Div-E"]
division_average_marks = [70,82,73,65,68]
variance = [5,8,7,6,4]

plt.barh(divisions, division_average_marks, xerr=variance, color="green")
plt.title("Bar Graph")
plt.xlabel("Marks")
plt.ylabel("Divisions")
plt.show()


# In[23]:


divisions = ["Div-A", "Div-B", "Div-C", "Div-D", "Div-E"]
division_average_marks = [70,82,73,65,68]
boys_average_marks = [68,67,77,61,70]

index = np.arange(5)
width = 0.30

plt.bar(index, division_average_marks, width, color="green",label="Division Marks")
plt.bar(index+width, boys_average_marks, width, color="blue", label"Boys Marks")
plt.title("Horizontally Stacked Bar Graphs")

plt.ylabel("Marks")
plt.xlabel("Divisions")
plt.xticks(index+ width/2, divisions)

plt.legend(loc="best")
plt.show()


# In[17]:


divisions = ["Div-A", "Div-B", "Div-C", "Div-D", "Div-E"]
boys_average_marks = [68,67,77,61,70]
girls_average_marks = [72,97,69,69,66]

index = np.arange(5)
width = 0.30

plt.bar(index, boys_average_marks, width, color="blue", label="Boys Marks")
plt.bar(index, girls_average_marks, width, color="red", label="Girls Marks", bottom=boys_average_marks)

plt.title("Vertically Stacked Bar Graphs")
plt.xlabel("Divisions")
plt.ylabel("Marks")
plt.xticks(index, divisions)

plt.legend(loc="best")
plt.show()


# In[18]:


firms = ["Firm A", "Firm B", "Firm C", "Firm D", "Firm E"]
market_share = [20,25,15,10,20]
Explode = [0,0.1,0,0,0]
plt.pie(market_share,explode=Explode,labels=firms,shadow=True,startangle=45)
plt.axis("equal")
plt.legend(title="List of Firms")
plt.show()


# In[19]:


x = np.random.rand(1000)

plt.title("Histogram")
plt.xlabel("Random Data")
plt.ylabel("Frequency")
plt.hist(x,10)
plt.show()


# In[20]:


height = np.array([167,170,149,165,155,180,166,146,159,185,145,168,172,181,169])
weight = np.array([86,74,66,78,68,79,90,73,70,88,66,84,67,84,77])

plt.xlim(140,200)
plt.ylim(60,100)
plt.scatter(height,weight)
plt.title("Scatter Plot")
plt.xlabel("Height")
plt.ylabel("Weight")
plt.show()

