#!/usr/bin/env python
# coding: utf-8

# In[8]:



# In[1]:


class Passenger:
    def __init__(self, fn, ln, validTicket):
        self.fn = fn
        self.ln = ln
        self.validTicket = validTicket

class Train:
    def __init__(self):
        self.passengerList = []
        
    def StepOut(self, passenger):
        self.passengerList.remove(passenger)
    
    def StepIn(self, passenger):
        self.passengerList.append(passenger)
    
        
    def PrintList(self):
        for passenger in self.passengerList:
            print(f'Firstname: {passenger.fn}; Lastname: {passenger.ln}')
    
class RailJet(Train):
    def __init__(self):
        Train.__init__(self)
    
    def StepIn(self, passenger):
        if (passenger.validTicket == True):
            Train.StepIn(self, passenger)
    
class InterCity(Train):
    def __init__(self, ICE_Category):
        Train.__init__(self)
        self.ICE_Category = ICE_Category

# Testing
raily = RailJet()
p1 = Passenger('Marcelo', 'Schullus', True)
p2 = Passenger('Lukas', 'Riederus', True)
p3 = Passenger('Andi', 'Arbeit', False)
p4 = Passenger('Fabio', 'Heizenberga', False)
p5 = Passenger('Krennus', 'Maximus', True)
p6 = Passenger('Grigory', 'Pernerstorfer', True)
p7 = Passenger('Sebastian', 'Zreiss', True)
# AAT 1
raily.StepIn(p1)
raily.StepIn(p2)
raily.StepIn(p3)
raily.StepIn(p4)
raily.StepIn(p5)
raily.StepIn(p6)
raily.StepIn(p7)
# AAT 2
print("===> First train ride: <===")
raily.PrintList()
# AAT 3
raily.StepOut(p1)
raily.StepOut(p7)
# AAT 4
print()
print("===> Second train ride: <===")
raily.PrintList()
# AAT 5
iC = InterCity("ICE2133")
p8 = Passenger ('Gluca', 'Markus', True)
p9 = Passenger ('Hans', 'Rauscher', False)
p10 = Passenger ('Abteilungssprecher', 'Strondl', True)
iC.StepIn(p8)
iC.StepIn(p9)
iC.StepIn(p10)
iC.StepOut(p9)
print()
print("===> Third train ride: ===")
iC.PrintList()


# In[ ]:





# In[ ]:





# In[ ]:




