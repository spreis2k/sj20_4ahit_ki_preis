#!/usr/bin/env python
# coding: utf-8

# # 1. Fill the missing pieces
# Fill the `____` parts in the code below.

# In[11]:


words = ['PYTHON', 'JOHN', 'chEEse', 'hAm', 'DOE', '123']
upper_case_words = []

for item in words:
    if item.isupper():
        upper_case_words.append(item)


# In[12]:


assert upper_case_words == ['PYTHON', 'JOHN', 'DOE']


# # 2. Calculate the sum of dict values
# Calculate the sum of the values in `magic_dict` by taking only into account numeric values (hint: see [isinstance](https://docs.python.org/3/library/functions.html#isinstance)). 

# In[13]:


magic_dict = dict(val1=44, val2='secret value', val3=55.0, val4=1)


# In[14]:


# Your implementation
sum_of_values = 0
for item in magic_dict.items():
    if isinstance(item[1], (int, float)):
        sum_of_values += item[1]


# In[15]:


assert sum_of_values == 100


# # 3. Create a list of strings based on a list of numbers
# The rules:
# * If the number is a multiple of five and odd, the string should be `'five odd'`
# * If the number is a multiple of five and even, the string should be `'five even'`
# * If the number is odd, the string is `'odd'`
# * If the number is even, the string is `'even'`

# In[17]:


numbers = [1, 3, 4, 6, 81, 80, 100, 95]


# In[16]:


# Your implementation
my_list = []
for item in numbers:
    if item % 5 == 0:
        if item % 2 == 0:
            my_list.append("five even")
        else:
            my_list.append("five odd")
    elif item % 2 == 0:
        my_list.append("even")
    else:
        my_list.append("odd")


# In[8]:


assert my_list == ['odd', 'odd', 'even', 'even', 'odd', 'five even', 'five even', 'five odd']


# In[ ]:




