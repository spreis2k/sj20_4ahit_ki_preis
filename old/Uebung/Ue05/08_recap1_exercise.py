#!/usr/bin/env python
# coding: utf-8

# # 1. Super vowels
# Implement `super_vowels` function which takes a string as an argument and returns a modified version of that string. In the return value of `super_vowels`, all vowels should be in upper case whereas all consonants should be in lower case. The vowels are listed in the `VOWELS` variable.

# In[1]:


VOWELS = ['a', 'e', 'i', 'o', 'u']


# In[2]:


# Your implementation here
def super_vowels(input):
    sentence = ''
    letter = ''
    for c in input:
        for vowel in VOWELS:
            if c.lower() == vowel.lower():
                letter = c.upper()
                break
            elif c.lower() != vowel.lower():
                letter = c.lower()
        sentence += letter
    return sentence


# In[3]:


assert super_vowels('hi wassup!') == 'hI wAssUp!'
assert super_vowels('HOw aRE You?') == 'hOw ArE yOU?'


# # 2. Playing board
# Implement `get_playing_board` function which takes an integer as an argument. The function should return a string which resemples a playing board (e.g. a chess board). The board should contain as many rows and columns as requested by the interger argument. See the cell below for examples of desired behavior.
# 

# In[17]:


# Your implementation here
def get_playing_board(num):
    x = 0
    y = 0
    output = ''
    while (y < num):
        x = 0
        while (x < num):
            if (x + y) % 2 == 0:
                output += ' '
            else:
                output += '*'
            x += 1
        output += "\n"
        y += 1
    return output


# In[18]:


board_of_5 = (
' * * \n'
'* * *\n'
' * * \n'
'* * *\n'
' * * \n'
)

board_of_10 = (
' * * * * *\n'
'* * * * * \n'
' * * * * *\n'
'* * * * * \n'
' * * * * *\n'
'* * * * * \n'
' * * * * *\n'
'* * * * * \n'
' * * * * *\n'
'* * * * * \n'
)

assert get_playing_board(5) == board_of_5
assert get_playing_board(10) == board_of_10

print(get_playing_board(50))

