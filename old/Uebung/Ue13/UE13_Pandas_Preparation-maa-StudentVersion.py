#!/usr/bin/env python
# coding: utf-8

# # UE 13 - Pandas Datenaufbereitung
# 
# Diese Abschnitt beschäftigt sich schwerpunktmäßig mit den Pandas-Methoden `map()`, `apply()` und `applymap()`. 
# 
# > **Hinweis**: Fügen Sie neue Pandas-Befehle dem FactSheet.pdf hinzu.
# 
# Als Übungsgrundlage dient das *Iris Data Set* (Quelle: https://archive.ics.uci.edu/ml/datasets/Iris). Hierbei handelt es sich um einen Datensatz, der drei Irisarten (Iris setosa, Iris virginica und Iris versicolor) - also Blüten - unterscheidet.

# In[ ]:


# Some libs are needed...


# ## 13.0 - Sklearn-Installation
# 
# Installieren Sie *Scikit Learn* (Sklearn) via *Anaconda Prompt* (Quelle: https://sklearn.org/install.html). Scikit ist eine frei Python-Bibliothek zum maschinellen Lernen, die auch gleich einige Datasets (Quelle: https://sklearn.org/modules/classes.html#module-sklearn.datasets) bereitstellt.

# ## 13.1 - Iris-Dataset laden
# Laden Sie das Iris Dataset aus dem Scikit Learn-Paket. Hierzu stellt Scikit mit `load_iris()` eine eigene Methode bereit. Bevor Sie das Dokument *Some notes to the iris dataset* (siehe htl.boxtree.at/lehre) durchnehmen, begründen Sie, warum a) `iris.shape` dem Interprete nicht schmeckt?
# 
# Nehmen Sie b) das oben erwähnte Dokument durch und probieren Sie sich an der Variable `iris`.

# In[1]:


import sklearn
from sklearn import datasets

iris = datasets.load_iris()
iris
#iris.shape
#iris ist kein DataFrame oder Series - es ist ein Objekt der Klasse "Bunch"
# Your Code...
print(iris.keys())
print(iris.target)
print(iris.target_names)
print(iris.frame)
print(iris.feature_names)
print(iris.DESCR)


# ## 13.2 - DataFrame erstellen
# 
# Erstellen Sie a) das DataFrame `df_data` und geben Sie die ersten 5 Zeilen aus. Gefordert ist folgender Aufbau:
# 
# ```Python
#        sepal length (cm) 	sepal width (cm) 	petal length (cm) 	petal width (cm)
#     0 	   5.1 	                 3.5 	              1.4 	             0.2
#     1 	   4.9 	                 3.0 	              1.4 	             0.2
#     2 	   4.7 	                 3.2 	              1.3 	             0.2
#     3 	   4.6 	                 3.1 	              1.5 	             0.2
#     4 	   5.0 	                 3.6 	              1.4 	             0.2
# ```

# In[2]:


#Your code...
import pandas as pd

df_data = pd.DataFrame(iris.data, columns=iris.feature_names)
df_data.head(5)


# Fügen Sie b) dem DataFrame `df_data` die Spalte *Species* hinzu und erstellen Sie ein neues DataFrame mit dem Namen `iris_df`. Das gesuchte Ergebnis:
# 
# ```Python
#        sepal length (cm) 	sepal width (cm) 	petal length (cm) 	petal width (cm)       Species
#     0 	   5.1 	                 3.5 	              1.4 	             0.2               0
#     1 	   4.9 	                 3.0 	              1.4 	             0.2               0
#     2 	   4.7 	                 3.2 	              1.3 	             0.2               0
#     3 	   4.6 	                 3.1 	              1.5 	             0.2               0
#     4 	   5.0 	                 3.6 	              1.4 	             0.2               0
# ```
# 
# 

# In[3]:


#Your Code...
df_data["Species"] = iris.target
iris_df = df_data.copy()
iris_df.head(5)
#Lösung vom Prof
# df_target = pd.DataFrame(iris['target'], columns=['Species'])
#iris_df = pd.concat([df_data, df_target], axis=1)
#iris_df.head()


# c) Verschaffen Sie sich einen Überblick, indem Sie folgende Fragen beantworten, und zwar auf Code-Eben:
# 
# - Über wie viele Zeilen verfügt das DataFrame `iris_df`?
# - Wie viele unterschiedliche Arten (Species) gibt es und wie viele umfasst die jeweilige Art?
# - Wie viele Zellen weisen `nan` auf?
# - Beurteilen Sie, ob die Mittelwertbildung der Spalte 'Species' Sinn ergibt.
# - Finden Sie heraus, ob eine Korrelation zwischen einzelen Features (Sepal length,..., Petal width) besteht.

# In[4]:


#Your Code....
#1)
iris_df.shape #150 rows
#2)
print(iris_df['Species'].unique().sum())
print(iris_df.groupby(['Species'])['Species'].count())
#3)
print(iris_df.isnull().sum().sum())
#4)
print("Nö ergibt keinen Sinn")
#5)
print(iris_df.corr())


# ## 13.3 - apply, applymap und map
# 
# Arbeiten Sie das Tutorial https://towardsdatascience.com/introduction-to-pandas-apply-applymap-and-map-5d3e044e93ff  durch. Abgabe der Code-Beispiele ist nicht notwendig.

# In[ ]:


# Playground...


# ## 13.4 Data preparation with apply, applymap or map
# 
# Überschreiben Sie a) die Spalte *Species*, wobei folgende Zuordnung gilt:
# 
# - 0 => SET
# - 1 => VER
# - 2 => VIR
# 
# Gesuchte Ergebnis:
# ```Python
#        sepal length (cm) 	sepal width (cm) 	petal length (cm) 	petal width (cm)       Species
#     0 	   5.1 	                 3.5 	              1.4 	             0.2               SET
#     1 	   4.9 	                 3.0 	              1.4 	             0.2               SET
#     2 	   4.7 	                 3.2 	              1.3 	             0.2               SET
#     3 	   4.6 	                 3.1 	              1.5 	             0.2               SET
#     4 	   5.0 	                 3.6 	              1.4 	             0.2               SET
# ```

# In[38]:


#Your Code...
def willnimma(val):
    if(val == 0):
        return "SET"
    if(val == 1):
        return "VER"
    if(val == 2):
        return "VIR"
    
iris_df['Species'] = iris_df['Species'].apply(willnimma)
iris_df
#iris_df['Species'] = iris_df['Species'].map({0:'SET', 1:'VER', 2:'VIR'})
#iris_df


# Erstellen Sie b) die neue Spalte `wide petal`, die das Ergebnis folgender Bedingung enhält:
# 
# Wenn `petal width (cm) >= 1.3` ist, dann soll die Zelle der jeweiligen Zeile den Wert 1 aufweisen, sonst 0. Setzen Sie eine `lambda`-Expression ein.

# In[39]:


#Your code...
iris_df['wide petal'] = iris_df['petal width (cm)'].apply(lambda x: 1 if(x>=1.3) else 0)
iris_df


# Ermitteln Sie c) die *petal area* (Petal-Fläche) und speichern Sie diese in der neu zu erstellenden Spalte *petal area*. Setzen Sie eine `lambda`-Expression ein. 

# In[40]:


# You Code...

iris_df = iris_df.assign(petalarea=lambda x: (x['petal length (cm)'] * x['petal width (cm)']))
iris_df
#iris_df['petal area'] = iris_df.apply(lambda row: (row['petal lengt'] * row['petal width']), axis=1)
#iris_df


# In[41]:


import numpy as np


# Logarithmieren Sie d) alle jene Zellen, die vom Typ `float`sind. Verwenden Sie `np.log()`. `lambda` is still your friend!

# In[42]:


# Your Code...
iris_df_test = iris_df.applymap(lambda x: np.log(x) if type(x) == float else x)
iris_df_test
#iris_df.applymap(lambda value: np.log(value) if isinstance(value, np.float) else value) 


# In[ ]:




