#!/usr/bin/env python
# coding: utf-8

# In[44]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.metrics import plot_confusion_matrix
from sklearn.preprocessing import StandardScaler
get_ipython().run_line_magic('matplotlib', 'inline')

#--------------------------------------explorative Datenanalyse-----------------------------------
diabetes = pd.read_csv('diabetes.csv')


# In[17]:


diabetes.columns


# In[15]:


diabetes.head()


# In[18]:


print("\n Dimensionen des Datasets: {}".format(diabetes.shape))


# In[19]:


print("Wieviele haben Diabetes: ")
print(diabetes.groupby('Outcome').size())


# In[25]:


sns.countplot(diabetes['Outcome'],label="Count")


# In[24]:


#sns.heatmap(diabetes,cmap="Blues",annot=False)
sns.heatmap(diabetes.corr(),cmap="Blues")


# In[65]:


#https://likegeeks.com/seaborn-heatmap-tutorial/
#https://datascienceplus.com/machine-learning-for-diabetes-with-python/
#https://medium.com/@szabo.bibor/how-to-create-a-seaborn-correlation-heatmap-in-python-834c0686b88e
#https://www.kaggle.com/baiazid/pima-indians-diabetes-svm

#---------------------------------------------Random Forest Classifier----------------------------------------
clf = RandomForestClassifier(max_depth=2, n_estimators=11)
X = diabetes[['Pregnancies','Glucose','BloodPressure','SkinThickness','Insulin','BMI','DiabetesPedigreeFunction','Age']]
y = diabetes[['Outcome']].to_numpy().ravel()


# In[66]:


X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1,test_size=0.30)
clf.fit(X_train,y_train)


# In[68]:


y_pred=clf.predict(X_test)
plot_confusion_matrix(clf, X_test, y_test)


# In[73]:


#------------------------------------------------Support Vector Machine (SVM)-----------------------------------
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1, test_size=0.3)
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

classifier = SVC(random_state=0, kernel='rbf')
classifier.fit(X_train, y_train)


# In[78]:


y_pred = classifier.predict(X_test)
confusion_matrix(y_test, y_pred)

