#!/usr/bin/env python
# coding: utf-8

# # 17.3.1 Kreuzen Sie an:
# ##### Körpergewicht: quantitatives Merkmal
# ##### Autobahnbezeichnung: qualitatives M.
# ##### Lieblingsfarbe: qualitatives M.
# ##### Taschengeld: quantitatives M.
# ##### Hausnummer: qualitatives M. 
# 

# # 17.3.2 Klassifizieren der Daten
# ##### country: qualitatives M.
# ##### beer_servings: quantitatives M.
# ##### spirit_servings: quantitatives M.
# ##### wine_servings: quantitatives M.
# ##### total_litres_of_pure_alcohol: quantitatives M.
# ##### continent: qualitatives M. 

# # 17.3.3 Erstellung einer neuen Spalte + Befüllen

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


diabetes_df = pd.read_csv("diabetes.csv")
diabetes_df["Demo"] = np.random.choice(["A", "B", "C"], diabetes_df.shape[0])
diabetes_df


# In[3]:


diabetes_df["Demo"].describe()


# In[4]:


object_to_category = {'Demo': "category"}
diabetes_df = diabetes_df.astype(object_to_category)
diabetes_df.info()


# In[3]:


diabetes_df.describe(include=["number", "category"])


# In[ ]:




