#!/usr/bin/env python
# coding: utf-8

# In[5]:


import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import seaborn as sns
from sklearn import svm
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import plot_confusion_matrix
from sklearn import datasets
from sklearn.model_selection import GridSearchCV


# # 17.1.1

# In[6]:



diabetes_df = pd.read_csv("diabetes.csv")
print(f"Zeilen / Reihen \n", diabetes_df.shape)
print("-------------------------------------------------------------")
print(f"Der Durchschnitt aller Werte\n", diabetes_df.mean())
print("-------------------------------------------------------------")
print(f"Gesamtanzahl an Pregnancies: ", diabetes_df['Pregnancies'].sum())
print("-------------------------------------------------------------")
print(f"Glucose-Werte \n", diabetes_df['Glucose'])
print("-------------------------------------------------------------")
print(f"Durchschnitts-BloodPressure\n", diabetes_df['BloodPressure'].mean())
print("-------------------------------------------------------------")
print(f"Der höchste Wert an SkinThickness: ", diabetes_df['SkinThickness'].max())
print("-------------------------------------------------------------")
print(f"Zusammenfassung der Werte von Insulin: \n", diabetes_df['Insulin'].describe())
print("-------------------------------------------------------------")
print(f"Durchschnittlicher BMI Wert: ", diabetes_df['BMI'].mean())
print("-------------------------------------------------------------")
print(f"DiabetesPedigreeFunction:", diabetes_df['DiabetesPedigreeFunction'].std())
print("-------------------------------------------------------------")
print(f"Der jüngste Patient:", diabetes_df['Age'].min())
print("-------------------------------------------------------------")
print(f"Es sind xxx viele betroffen: ", diabetes_df[diabetes_df['Outcome']==1].count())
diabetes_df.head()


# # 17.1.2

# In[7]:



sns.countplot(diabetes_df['Outcome'])


# # 17.1.3

# In[8]:


sns.heatmap(diabetes_df.corr())


# # 17.1.4

# In[9]:


x = diabetes_df.iloc[:, :-1]
y = diabetes_df["Outcome"]

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.2, random_state=1)


# ## RandomForestClassifier

# In[10]:


#RandomForestClassifier
clf = RandomForestClassifier(max_depth=5, n_estimators=200, random_state=1)
clf.fit(x_train,y_train)
y_pred=clf.predict(x_test)
print(accuracy_score(y_test, y_pred)*100)


# ## SVC 

# In[11]:


#SVC Classifier
cls = svm.SVC(kernel="rbf", random_state=1)
cls.fit(x_train, y_train)
predicted = cls.predict(x_test)
print(accuracy_score(y_test, y_pred)*100)


# In[ ]:





# In[ ]:





# In[ ]:





# # Übung 17.2

# # 17.2.1

# In[12]:


param_grid = {
    'bootstrap': [True],
    'max_depth': [80, 90, 100],
    'max_features': [2, 3],
    'min_samples_leaf': [3, 4, 5],
    'min_samples_split': [8, 10, 12],
    'n_estimators': [100, 150, 200]
}
grid_search = GridSearchCV(estimator = clf, param_grid = param_grid, n_jobs = -1, verbose = 2)


# In[13]:


grid_search.fit(x_train, y_train)


# In[14]:


print(grid_search.best_params_)
print(grid_search.best_score_)


# # 17.2.2

# In[15]:


diabetes_df_0 = diabetes_df.loc[diabetes_df["Outcome"] == 0]
diabetes_df_1 = diabetes_df.loc[diabetes_df["Outcome"] == 1]

insulin_median_0 = diabetes_df_0["Insulin"].median(axis=0)
insulin_median_1 = diabetes_df_1["Insulin"].median(axis=0)
skin_median_0 = diabetes_df_0["SkinThickness"].median(axis=0)
skin_median_1 = diabetes_df_1["SkinThickness"].median(axis=0)

diabetes_df_2 = diabetes_df.copy()
diabetes_df_2.loc[np.logical_and(diabetes_df_2.Insulin == 0, diabetes_df_2.Outcome == 0), 'Insulin'] = insulin_median_0
diabetes_df_2.loc[np.logical_and(diabetes_df_2.Insulin == 0, diabetes_df_2.Outcome == 1), 'Insulin'] = insulin_median_1
diabetes_df_2.loc[np.logical_and(diabetes_df_2.SkinThickness == 0, diabetes_df_2.Outcome == 0), 'SkinThickness'] = skin_median_0
diabetes_df_2.loc[np.logical_and(diabetes_df_2.SkinThickness == 0, diabetes_df_2.Outcome == 1), 'SkinThickness'] = skin_median_1
diabetes_df_2


# # 17.2.3
# ## Dataset mit den neuen Werten

# In[16]:


x_2 = diabetes_df_2.iloc[:, :-1]
y_2 = diabetes_df_2["Outcome"]

x_train_2, x_test_2, y_train_2, y_test_2 = train_test_split(x_2, y_2, test_size=.2, random_state=1)


# In[17]:


grid_search_2 = GridSearchCV(estimator = clf, param_grid = param_grid, n_jobs = -1, verbose = 2)

grid_search_2.fit(x_train_2,y_train_2)


# In[18]:


print(grid_search_2.best_params_)
print(grid_search_2.best_score_)


# ## SVM

# In[19]:


#SVM
cls = svm.SVC(kernel="rbf", random_state=1)
param_grid_2 = [
  {'C': [0.001, 0.01, 0.1, 1], 'gamma': [0.001, 0.01, 0.1, 1], 'kernel': ['rbf']}
 ]

grid_search_svc_2 = GridSearchCV(cls, param_grid_2)
grid_search_svc_2.fit(x_train_2,y_train_2)

print(grid_search_svc_2.best_params_)
print(grid_search_svc_2.best_score_)


# # 17.2.4

# In[20]:


plot_confusion_matrix(clf, x_test_2, y_test_2, labels=[1,0])


# In[45]:


plot_confusion_matrix(grid_search_svc_2, x_test_2, y_test_2, labels=[1,0])

