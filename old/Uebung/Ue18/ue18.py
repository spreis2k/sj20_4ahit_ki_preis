#!/usr/bin/env python
# coding: utf-8

# In[2]:


from tensorflow.keras.datasets import mnist
import pandas as pd
import matplotlib.pyplot


# In[8]:


#18.1
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


# In[44]:


#18.2
data = pd.DataFrame(train_images.reshape(train_images.shape[0],784))
data


# In[2]:


18.3
matplotlib.pyplot.imshow(train_images[0], cmap="gray_r")


# In[ ]:




