#!/usr/bin/env python
# coding: utf-8

# # UE18.3 - MNIST3

# In[4]:


import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import load_model 
from sklearn.metrics import confusion_matrix
import pandas as pd
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


# In[5]:


(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


# In[6]:


train_images_df = np.array(train_images)
train_images_df = train_images_df.reshape((1, 60000, 784))
df = pd.DataFrame(train_images_df[0])
df.head()


# In[7]:


model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(512, activation="relu", input_shape=(784,)))
model.add(tf.keras.layers.Dense(10, activation="softmax"))
#model.add(tf.keras.layers.Dense(512, activation="relu"))
model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])
model.summary()


# In[8]:


train_labels = tf.keras.utils.to_categorical(train_labels, 10)
train_labels.shape


# In[9]:


train_images = train_images.reshape((-1, 784))


# In[10]:


#model.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[11]:


model_history = model.fit(train_images, train_labels, epochs=10, batch_size=64)
type(model_history.history)


# In[12]:


acc = model_history.history["accuracy"]
acc


# ## 18.2.1 Plot erstellen

# In[13]:


plt.plot(acc, 'b', label="Training")
plt.title("Korrektklassifizierungsrate Training")
plt.xlabel("Epochen")
plt.ylabel("Korrektklassifizierungsrate")
plt.legend()
plt.show()


# ## 18.2.2 

# In[14]:


test_images = test_images.reshape(test_images.shape[0], 784)
test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)


# In[15]:


hist = model.fit(train_images, train_labels, validation_data=(test_images, test_labels), epochs=10, batch_size=128)


# In[16]:


hist.history.keys()


# In[ ]:





# In[17]:


plt.ylim((0.9,1))
plt.plot(hist.history["accuracy"], 'b', label="Training") 
plt.plot(hist.history["val_accuracy"], 'r', label="Validierung") 
plt.title("Korrektklassifizierungsrate Training/Validierung") 
plt.xlabel("Epochen") 
plt.ylabel("Korrektklassifizierungsrate") 
plt.legend()
plt.show()


# ## 18.3.1

# In[18]:


y_pred = model.predict(test_images)
y_pred = np.argmax(y_pred, axis=1)
test_labels = np.argmax(test_labels, axis=1)

pd.crosstab(test_labels, y_pred, rownames=["actual"], colnames=["predicted"])


# ## 18.3.2

# In[19]:


train_images[0]


# In[20]:


train_images = train_images / 255
train_images[0]


# ## 18.3.3
# 

# In[21]:


(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


# In[22]:


train_images = train_images / 255


# In[23]:


test_images = test_images.reshape(test_images.shape[0], 784)
test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)
train_labels = tf.keras.utils.to_categorical(train_labels, 10)
train_images = train_images.reshape((-1, 784))


# In[24]:


model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(512, activation="relu", input_shape=(784,)))
model.add(tf.keras.layers.Dense(10, activation="softmax"))
#model.add(tf.keras.layers.Dense(512, activation="relu"))
model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])
model.summary()
hist = model.fit(train_images, train_labels, validation_data=(test_images, test_labels), epochs=5, batch_size=128)


# In[25]:


plt.ylim((0.9,1))
plt.plot(hist.history["accuracy"], 'b', label="Training") 
plt.plot(hist.history["val_accuracy"], 'r', label="Validierung") 
plt.title("Korrektklassifizierungsrate Training/Validierung") 
plt.xlabel("Epochen") 
plt.ylabel("Korrektklassifizierungsrate") 
plt.legend()
plt.show()


# ## 18.3.4

# In[26]:


model.save('my_model.h5')


# In[27]:


model = load_model('my_model.h5')


# In[ ]:




