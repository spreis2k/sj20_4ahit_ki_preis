#!/usr/bin/env python
# coding: utf-8

# ## 21.1

# In[2]:


from PIL import Image, ImageDraw
import face_recognition


# In[18]:


# Load the jpg file into a numpy array
image = face_recognition.load_image_file("data/biden.jpg")


# In[3]:


# Find all the faces in the image using the default HOG-based model.
# This method is fairly accurate, but not as accurate as the CNN model and
# See also: find_faces_in_picture_cnn.py
face_locations = face_recognition.face_locations(image)


# In[4]:


print("I found {} face(s) in this photograph.".format(len(face_locations)))


# In[5]:


for face_location in face_locations:

    # Print the location of each face in this image
    top, right, bottom, left = face_location
    print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))


# In[19]:


# You can access the actual face itself like this:
face_image = image[top:bottom, left:right]
pil_image = Image.fromarray(face_image)
pil_image.show()


# ## 21.2

# In[20]:


image = face_recognition.load_image_file("data/pexels-fauxels-3184419.jpg")

face_locations = face_recognition.face_locations(image)
print("Auf diesem Bild sind {} zu sehen.".format(len(face_locations)))
print(face_locations)


# ## 21.3

# In[8]:


# Load the jpg files into numpy arrays
biden_image = face_recognition.load_image_file("data/biden.jpg")
obama_image = face_recognition.load_image_file("data/obama.jpg")
unknown_image = face_recognition.load_image_file("data/obama2.jpg")


# In[9]:


# Get the face encodings for each face in each image file
# Since there could be more than one face in each image, it returns a list of encodings.
# But since I know each image only has one face, I only care about the first encoding in each image, so I grab index 0.
try:
    biden_face_encoding = face_recognition.face_encodings(biden_image)[0]
    obama_face_encoding = face_recognition.face_encodings(obama_image)[0]
    unknown_face_encoding = face_recognition.face_encodings(unknown_image)[0]
except IndexError:
    print("I wasn't able to locate any faces in at least one of the images. Check the image files. Aborting...")
    quit()
    
known_faces = [
    biden_face_encoding,
    obama_face_encoding
]


# In[10]:


# results is an array of True/False telling if the unknown face matched anyone in the known_faces array
results = face_recognition.compare_faces(known_faces, unknown_face_encoding)

print("Is the unknown face a picture of Biden? {}".format(results[0]))
print("Is the unknown face a picture of Obama? {}".format(results[1]))
print("Is the unknown face a new person that we've never seen before? {}".format(not True in results))


# ## 21.4

# In[22]:


# Load the jpg file into a numpy array
image = face_recognition.load_image_file("data/putin.jpg")


# In[1]:


# Find all facial features in all the faces in the image
face_landmarks_list = face_recognition.face_landmarks(image)
face_landmarks_list


# In[24]:


pil_image = Image.fromarray(image)
for face_landmarks in face_landmarks_list:
    d = ImageDraw.Draw(pil_image, 'RGBA')

    # Make the eyebrows into a nightmare
    d.polygon(face_landmarks['left_eyebrow'], fill=(68, 54, 39, 128))
    d.polygon(face_landmarks['right_eyebrow'], fill=(68, 54, 39, 128))
    d.line(face_landmarks['left_eyebrow'], fill=(68, 54, 39, 150), width=5)
    d.line(face_landmarks['right_eyebrow'], fill=(68, 54, 39, 150), width=5)

    # Gloss the lips
    d.polygon(face_landmarks['top_lip'], fill=(150, 0, 0, 128))
    d.polygon(face_landmarks['bottom_lip'], fill=(150, 0, 0, 128))
    d.line(face_landmarks['top_lip'], fill=(150, 0, 0, 64), width=8)
    d.line(face_landmarks['bottom_lip'], fill=(150, 0, 0, 64), width=8)

    # Sparkle the eyes
    d.polygon(face_landmarks['left_eye'], fill=(255, 255, 255, 30))
    d.polygon(face_landmarks['right_eye'], fill=(255, 255, 255, 30))

    # Apply some eyeliner
    d.line(face_landmarks['left_eye'] + [face_landmarks['left_eye'][0]], fill=(0, 0, 0, 110), width=6)
    d.line(face_landmarks['right_eye'] + [face_landmarks['right_eye'][0]], fill=(0, 0, 0, 110), width=6)

    pil_image.show()
    

