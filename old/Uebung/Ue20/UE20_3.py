#!/usr/bin/env python
# coding: utf-8

# # Imports

# In[1]:


import os
import tensorflow as tf
from keras_preprocessing.image import ImageDataGenerator
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten
from tensorflow.keras.utils import to_categorical
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tensorflow.keras.preprocessing.image import load_img
from os import listdir
from os.path import isfile, join
from tensorflow.python.keras.layers import MaxPooling2D


# # UE 20.3 Convolutional Neuronal Networks 3 Cats and Dogs
# ## 20.3.1

# In[3]:


train = os.listdir('./train/')
test = os.listdir("./test1")
train_images_df= pd.DataFrame(train, columns=["filename"])
train_images_df["category"]= train_images_df["filename"].str.split(".", n=1,expand = True)[0]
print(train_images_df)


# ## 20.3.2

# In[8]:


print(train_images_df.shape[0])
print(train_images_df['category'].value_counts())


# ## 20.3.3

# In[5]:


def show_train_image(filename):
    img_file = load_img('./train/'+filename)
    plt.imshow(img_file)


# In[6]:


show_train_image('cat.0.jpg')

