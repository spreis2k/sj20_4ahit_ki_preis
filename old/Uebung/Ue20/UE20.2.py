#!/usr/bin/env python
# coding: utf-8

# # Imports

# In[1]:


import numpy as np
import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten,MaxPooling2D
from tensorflow.keras.utils import to_categorical
from sklearn.metrics import accuracy_score
import tensorflow.keras.backend as K
import matplotlib.pyplot as plt


# In[2]:


physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)


# # UE 20.2 Convolutional Neuronal Networks 2
# ## 20.2.1

# In[3]:




(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


train_images = train_images.reshape(train_images.shape[0],train_images.shape[1],train_images.shape[2],1)
test_images = test_images.reshape(test_images.shape[0], test_images.shape[1], test_images.shape[2], 1)

train_images = train_images / 255
test_images = test_images / 255

train_labels = to_categorical(train_labels, 10)


# In[4]:




model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu",input_shape=(28,28,1)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu",))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu",))
model.add(Flatten())

model.add(Dense(10, activation="softmax"))
model.compile(optimizer="rmsprop", loss="categorical_crossentropy",metrics=["accuracy"])


# In[5]:


#Trainieren:
model.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[6]:



predicted_minst_numbers = model.predict(test_images)
predicted_minst_numbers = np.argmax(predicted_minst_numbers, axis=1)

print(f"MINST Accuracy: {accuracy_score(test_labels, predicted_minst_numbers) * 100}%")


# ## 20.2.2

# In[7]:




modelWithDense = Sequential()
modelWithDense.add(Conv2D(10, kernel_size=(3,3), activation="relu",input_shape=(28,28,1)))
modelWithDense.add(MaxPooling2D(pool_size=(2,2)))
modelWithDense.add(Conv2D(10, kernel_size=(3,3), activation="relu",))
modelWithDense.add(MaxPooling2D(pool_size=(2,2)))
modelWithDense.add(Conv2D(10, kernel_size=(3,3), activation="relu",))
modelWithDense.add(Flatten())
modelWithDense.add(Dense(64, activation="relu"))
modelWithDense.add(Dense(10, activation="softmax"))
modelWithDense.compile(optimizer="rmsprop", loss="categorical_crossentropy",metrics=["accuracy"])


# In[8]:


#Training:
modelWithDense.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[9]:



predicted_minst_numbers_withDense = modelWithDense.predict(test_images)
predicted_minst_numbers_withDense = np.argmax(predicted_minst_numbers_withDense, axis=1)
#Accuracy überprüfen:
print(f"MINST Accuracy mit Dense Layer: {accuracy_score(test_labels, predicted_minst_numbers_withDense) * 100}%")


# ### Wie man sehen kann wird das Ergebnis durch den Dense Layer um ungefähr ein halbes Prozent verbessert. Da hier aber nur mit wenigen Epochen trainiert wurde ist der Unterschied womöglich deshalb so niedrig ausgefallen

# ## 20.2.3 Interpretation:

# In[10]:


model.summary();


# 
# 
