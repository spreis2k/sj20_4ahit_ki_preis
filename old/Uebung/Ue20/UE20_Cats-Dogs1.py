#!/usr/bin/env python
# coding: utf-8

# # Convolutional Neuronal Networks 3
# 

# In[26]:


from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten, MaxPooling2D
from tensorflow.keras.utils import to_categorical
import tensorflow.keras.backend as K 
from tensorflow.keras.preprocessing import image
from tensorflow.keras.datasets import mnist
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import os
from PIL import Image
import glob
from tensorflow.keras.preprocessing.image import ImageDataGenerator


# ## 20.3.1

# In[3]:


img = Image.open("train/cat.1.jpg")
img


# In[27]:


img_array = []
files = os.listdir("train")
#for my_file in files:
    #print(my_file)    
    #image = Image.open(my_file).convert("L")
    #image = np.array(image)
    #image = image.reshape(784)    
    #img_array.append(image)
#img_array = np.array(img_array)    
#print('img_array shape:', np.array(img_array).shape)


# In[30]:


def CatsDogs(row):
    if(row["filename"].split(".")[0]  == "cat"):
        return "cat"
    elif(row["filename"].split(".")[0] == "dog"):
        return "dog"

data_df = pd.DataFrame(files,columns=['filename'])
data_df["category"] = data_df.apply(CatsDogs, axis=1)
data_df


# ## 20.3.2

# In[11]:


print(f"Anzahl an Datensätzen: {data_df.shape[0]}")
print(f"Anzahl an Katzen: {data_df[data_df['category']== 'cat'].shape[0]}")
print(f"Anzahl an Hunden: {data_df[data_df['category'] == 'dog'].shape[0]}")


# ## 20.3.3

# In[7]:


def ShowPets(pets, number):
    pic = image.load_img("train\\" + pets + "." + str(number) + ".jpg")
    return pic

ShowPets("cat", 69)


# ## 20.3.4

# In[8]:


model = Sequential()
model.add(Conv2D(32, kernel_size=(3,3), activation="relu", input_shape=(150,150,3)))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64, kernel_size=(3,3), activation="relu", input_shape=(150,150,3)))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(128, kernel_size=(3,3), activation="relu", input_shape=(150,150,3)))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(128, kernel_size=(3,3), activation="relu", input_shape=(150,150,3)))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())
model.add(Dense(512, activation="relu"))
model.add(Dense(2, activation="softmax"))
model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[32]:


train_df, validation_df = train_test_split(data_df, test_size=0.20, random_state=12)
train_df = train_df.reset_index(drop=True)
validation_df = validation_df.reset_index(drop=True)
print(train_df.shape)
print(validation_df.shape)


# In[33]:


train_datagen = ImageDataGenerator(rescale=1./255)
validation_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_dataframe(dataframe = train_df, directory = "train", x_col = "filename", y_col = "category",
target_size = (150,150), batch_size = 200, class_mode = "categorical")

validation_generator = validation_datagen.flow_from_dataframe(dataframe = validation_df, directory = "train", x_col = "filename",
y_col = "category", target_size = (150,150), batch_size = 200, class_mode = "categorical")


# In[35]:


for data_batch, labels_batch in train_generator:
    print(data_batch.shape)
    print(labels_batch.shape)
    break


# In[ ]:





# In[36]:


history = model.fit(train_generator, steps_per_epoch = 100, validation_data = validation_generator, epochs=3)


# In[37]:


test_filenames = os.listdir("test1")
test_df = pd.DataFrame({'filename': test_filenames})
test_df.shape[0]


# In[38]:


test_gen = ImageDataGenerator(rescale=1./255)
test_generator = test_gen.flow_from_dataframe(
test_df,
directory="test1",
x_col='filename',
y_col=None,
class_mode=None,
target_size=(150,150),
batch_size=32,
shuffle=False
)


# In[40]:


test_generator.reset()
pred=model.predict(test_generator, verbose=1)
pred


# In[41]:


pred_rounded = np.argmax(pred, axis=-1)
test_df["category"] = pred_rounded
test_df.head(10)


# In[43]:


sample_test = test_df.head(18)
sample_test.head()
plt.figure(figsize=(12, 24))
for index, row in sample_test.iterrows():
    filename = row['filename']
    category = row['category']
    img = image.load_img("test1/"+filename, target_size=(150,150))
    plt.subplot(6, 3, index+1)
    plt.imshow(img)
    plt.xlabel(f"{filename} ({category})")
plt.tight_layout()
plt.show()


# In[44]:


train_generator.class_indices.items()

