#!/usr/bin/env python
# coding: utf-8

# In[11]:


import pandas as pd
import numpy as np


# ## Zum Aufwärmen

# ##### Numpy Array mit Inhalt: array([1,2,3,4,5,6,7,8,9])

# In[12]:


array = np.array([*range(1,10,1)])
array


# ##### Ändern Sie die Gestalt des Arrays

# In[13]:


array = np.reshape(array, (3,3))
array


# ##### Spaltenweiser Mittelwert

# In[14]:


np.mean(array, axis=0)


# ## Convolutional Neural Networks 1

# In[5]:


from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.datasets import mnist
import matplotlib.pyplot as plt
import tensorflow.keras as keras


# In[6]:


(X_train, y_train), (X_test, y_test) = mnist.load_data()

plt.imshow(X_train[0], cmap='gray_r')

X_train = X_train.reshape(60000,28,28,1)
X_test = X_test.reshape(10000, 28, 28, 1)

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

y_train[0]


# In[7]:


model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu",
                input_shape =(28,28,1)))
model.add(Flatten())
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])

model.fit(X_train, y_train, epochs=10, batch_size=1000)


# Bei RGB bräuchte man den für jeden Farbraum (Rot, Grün, Blau) jeweils 255 Werte

# In[8]:


model.layers


# In[ ]:





# In[140]:


import tensorflow.keras.backend as K
data = K.eval(model.layers[0].weights[0])
data


# In[16]:


data.shape


# In[143]:


plt.imshow(data[:,:,0,1].reshape(3,3))
plt.show()


# In[146]:


for i in range(10):
    plt.subplot(6,6,i+1)
    plt.imshow(data[:,:,0,i]); 
    plt.axis('off');

