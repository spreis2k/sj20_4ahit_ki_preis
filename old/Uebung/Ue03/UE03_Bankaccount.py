#!/usr/bin/env python
# coding: utf-8

# In[1]:


one = 1


# In[1]:


class BankAccount:
    def __init__(self):
        self._balance = 0
        self._isFrozenBoolean = False
        
        def withdraw(self, amount):
        if (not self._isFrozenBoolean) & (self._balance - amount >= -1000):
            self._balance -= amount
            return self._balance
        elif (not self._isFrozenBoolean) & (self._balance - amount < -1000):
            return 'Your balance cannot exceed below -1000'
        else:
            return 'Your account is frozen'
    
    
    def deposit(self, amount):
        if (not self._isFrozenBoolean) & (amount > 0):
            self._balance += amount
            return self._balance
        elif amount < 0:
            return 'You can not deposit negative amounts'
        else:
            return 'Your account is frozen'
        
    
        
    def freezeAccount(self):
        self._isFrozenBoolean = True
        
    def getBalance(self):
        return self._balance
        

        
        


# In[7]:


acc1 = BankAccount()
print(acc1.deposit(700))
print(acc1.freezeAccount())
print(acc1.withdraw(2200))
print (acc1.getBalance())


# In[ ]:




