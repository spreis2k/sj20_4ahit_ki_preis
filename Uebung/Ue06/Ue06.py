# %%
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import spacy
import pandas as pd
nlp = spacy.load("en_core_web_lg")

# %%
def calculate_similarity(text1, text2):
    text1 = nlp(text1)
    text2 = nlp(text2)
    return text2.similarity(text1) 


# %%
df = pd.read_table("covid_faq.csv", sep=",")
df

# %%


# %%
question = "what do i do when i get covid?"
answer_list = {}
for answer in df['answers']:
    answer_list[answer] = calculate_similarity(question, answer)
    
answer_list

# %%
print(max((answer_list).values()), max(answer_list))


