#!/usr/bin/env python
# coding: utf-8

# In[1]:


import PyPDF2 as pypdf

# 2.1.
with open('Russische Revolution.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    docInfo = reader.documentInfo
    print(f'Author:\t\t{docInfo.author}')
    print(f'Producer:\t{docInfo.producer}')
    print(f'Title:\t\t{docInfo.title}')


# In[3]:


#2.2.
with open('Russische Revolution.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    
    # Nur 1. Seite:
    print(reader.getPage(0).extractText())
    print('\n\n\n\n\n_________________________________________________________________\n\n\n\n\n')
    
    # Alles ohne Liste:
    
    for i in range(reader.numPages):
        print(reader.getPage(i).extractText())
    
    # Alles mit Liste:
    print('\n\n\n\n\n_________________________________________________________________\n\n\n\n\n')
    output = []
    for i in range(reader.numPages):
        output.append(reader.getPage(i).extractText())
        
    print(output)


# In[4]:


####### -------------------------------------- #######
   #            Skript aus Angabedatei            #
####### -------------------------------------- #######
from docx import Document
from docx.shared import Inches
document = Document()
document.add_heading('Document Title', 0)
p = document.add_paragraph('A plain paragraph having some ')
p.add_run('bold').bold = True
p.add_run(' and some ')
p.add_run('italic.').italic = True
document.add_heading('Heading, level 1', level=1)
document.add_paragraph('Intense quote', style='Intense Quote')
document.add_paragraph(
'first item in unordered list', style='List Bullet'
)
document.add_paragraph(
'first item in ordered list', style='List Number'
)
records = (
(3, '101', 'Spam'),
(7, '422', 'Eggs'),
(4, '631', 'Spam, spam, eggs, and spam')
)
table = document.add_table(rows=1, cols=3)
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Qty'
hdr_cells[1].text = 'Id'
hdr_cells[2].text = 'Desc'
for qty, id, desc in records:
   row_cells = table.add_row().cells
   row_cells[0].text = str(qty)
   row_cells[1].text = id
   row_cells[2].text = desc
document.add_page_break()
document.save('demo.docx') 

# Bibliotheken laden
import urllib.request as urllib2
from bs4 import BeautifulSoup
# Website laden
response = urllib2.urlopen('https://www.htlkrems.ac.at')
html_doc = response.read()
# Das BeautifulSoup Object soup repräsentiert das „geparste“ HTML-Dokument
soup = BeautifulSoup(html_doc, 'html.parser')
# Das „geparste“ HTML-Dokument formatieren, sodass jeder Tag bzw. Textblock
# in einer separaten Zeile ausgegeben wird
strhtm = soup.prettify()
# Ein paar Zeilen ausgeben
print (strhtm[:1000]) 


# In[22]:


#2.3. Anzahl der Anchor tags
len(soup.find_all('a'))


# In[23]:


#2.4. Anzahl der Hyperlinks
links = []
for link in soup.findAll('a'):
    links.append(link.get('href'))

len(links)


# In[ ]:




