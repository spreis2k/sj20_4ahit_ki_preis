
# %Imports%
import nltk
nltk.download('vader_lexicon')

from nltk.sentiment.vader import SentimentIntensityAnalyzer
sid = SentimentIntensityAnalyzer()

import numpy as np
import pandas as pd

# %Einlesen von CSV%
df = pd.read_csv('areviews.tsv', sep='\t')
df.head()

# %DropNA ausführen%
df.dropna(inplace=True)

blanks = []

for i,lb, rv in df.itertuples():  
    if type(rv)==str:            
        if rv.isspace():        
            blanks.append(i)     

df.drop(blanks, inplace=True)

# %Dataframes anfügen%
df['scores'] = df['review'].apply(lambda review: sid.polarity_scores(review))
df['compound']  = df['scores'].apply(lambda score_dict: score_dict['compound'])
df['comp_score'] = df['compound'].apply(lambda c: 'pos' if c >=0 else 'neg')

df.head()

# %Werte zählen%
df['comp_score'].value_counts()
df['label'].value_counts()

# %imdb csv einlesen%
df_imdb = pd.read_csv('moviereviews.tsv', sep='\t')
df_imdb.head()

# %DropNA ausführen und durch die Tuples gehen%
df_imdb.dropna(inplace=True)

blanks_imdb = []

for i,lb, rv in df_imdb.itertuples():  
    if type(rv)==str:            
        if rv.isspace():        
            blanks_imdb.append(i)     

df_imdb.drop(blanks, inplace=True)

# %Appending%
df_imdb['scores'] = df_imdb['review'].apply(lambda review: sid.polarity_scores(review))
df_imdb['compound']  = df_imdb['scores'].apply(lambda score_dict: score_dict['compound'])
df_imdb['comp_score'] = df_imdb['compound'].apply(lambda c: 'pos' if c >=0 else 'neg')

df_imdb.head()

df_imdb['comp_score'].value_counts()

df_imdb['label'].value_counts()

# %Classification report%
from sklearn.metrics import classification_report
y_true = df_imdb['label']
y_pred = df_imdb['comp_score']

print(classification_report(y_true, y_pred))

# %Confusion Matrix%
from sklearn.metrics import confusion_matrix
y_true = df_imdb['label']
y_pred = df_imdb['comp_score']

print(confusion_matrix(y_true, y_pred))


