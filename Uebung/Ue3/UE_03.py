#!/usr/bin/env python
# coding: utf-8

# # UE3 Exploring and processing Data

# ## 3.2 LowerCase

# In[17]:


import pandas as pd

#3.2. lower-case
tweets=[
    'This is introduction to NLP',
    'It is likely to be useful, to people ',
    'Machine learning is the new electrcity',
    'There would be less hype around AI and more action going forward',
    'python is the best tool!','R is good langauage',
    'I like this book','I want more books like this'
]

df_Tweets = pd.DataFrame(tweets, columns=['tweets'])
df_Tweets['tweets'] = df_Tweets['tweets'].str.lower()

df_Tweets.head()


# ## 3.3 Satzzeichen entfernen

# In[18]:


# 3.3. Satzzeichen entfernen
df_Tweets['tweets'] = df_Tweets['tweets'].replace('[\.!,?]', '', regex=True)

df_Tweets.head()


# ## 3.4 NLTK & Split

# In[19]:


#3.4. nltk und split
import nltk
import numpy as np

  

new_tweets = ['this is introduction to nlp', 'it is likely to be useful to people ', 'machine learning is the new electrcity', 'there would be less hype around ai and more action going forward', 'python is the best tool', 'r is good langauage', 'i like this book', 'i want more books like this']
print('------------------------TOKENIZE---------------------')
for i in tweets:
    new_tweets.append(nltk.word_tokenize(i))
print(new_tweets)


new_tweets = ['this is introduction to nlp', 'it is likely to be useful to people ', 'machine learning is the new electrcity', 'there would be less hype around ai and more action going forward', 'python is the best tool', 'r is good langauage', 'i like this book', 'i want more books like this']
print('------------------------SPLIT---------------------')
for i in tweets:
    new_tweets.append(i.split())
print(new_tweets)

# Bei Tokenize werden Satzzeichen auch in eigene Array-Elemente getrennt, während bei Split nur nach Leerzeichen gesplitet wird.


# # 3.5 Tokenisierte Spalte machen

# In[20]:


# 3.5 Tokenisierte Spalte machen

df_Tweets['tokenized'] = df_Tweets['tweets'].str.split(' ')
df_Tweets.head()


# ## 3.6 Wörterzahl ermitteln

# In[21]:


#3.6 Wörterzahl ermitteln
tokenizer = nltk.RegexpTokenizer(r"\w+")

with open("dataScientistJobDesc.txt", "r") as lines:
    document_text = lines.read()
    
    tokenized_document = tokenizer.tokenize(document_text)
    # mit Duplikaten
    print(len(tokenized_document))
    
    document_set = set()
    document_set.update(tokenized_document)
    
    #ohne Duplikate
    print(len(document_set))


# ## 3.7 Wörterzahl ermitteln mit Spacy

# In[22]:


# 3.7 Selbiges mit Spacy
import spacy

    
nlp = spacy.load("de_core_news_sm")
doc = nlp(document_text)
# Länge mit Duplikaten
print(len(doc))
    
spacy_set = set()
spacy_set.update(f"{token.text:{20}}" for token in doc)

# Länge ohne Duplikate
print(len(spacy_set))


# ## 3.8 Wieso wird How nicht entfernt?

# In[23]:


# 3.8. Wieso wird How nicht entfernt? 
from nltk.corpus import stopwords as stopwords


print(stopwords.words('english'))


stop = stopwords.words('english')
text = "How to develop a chatbot in Python"

result = [word for word in text.split() if word not in stop]
print(f'\n\nOhne Lower-case: {result}')

result = [word for word in text.lower().split() if word not in stop]
print(f'Mit Lower-case: {result}')


# ## 3.9 stopwords in Dataframe

# In[24]:


#3.9. stopwords in Dataframe
tweets=[
    'This is introduction to NLP',
    'It is likely to be useful, to people ',
    'Machine learning is the new electrcity',
    'There would be less hype around AI and more action going forward',
    'python is the best tool!','R is good langauage',
    'I like this book','I want more books like this'
]

df_Tweets = pd.DataFrame(tweets, columns=['tweets'])
df_Tweets['tweets'] = df_Tweets['tweets'].str.lower()

df_Tweets['tweets'] = df_Tweets['tweets'].apply(lambda x: ' '.join([word for word in x.split() if word not in stop]))

df_Tweets.head()


# ## 3.10 PorterStemmer

# In[42]:


#3.10 PorterStemmer

from nltk.stem import PorterStemmer

text=['I like fishing','I eat fish','There are many fishes in pound']

pt = PorterStemmer()
[pt.stem(word) for word in text]


# ## 3.11 WordNetLemmatizer

# In[43]:


#3.11 WordNetLemmatizer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize

lemmatizer = WordNetLemmatizer()

[[lemmatizer.lemmatize(word) for word in word_tokenize(t)] for t in text]


# # Exploring and processing Data II

# ## Preprocess Pipeline

# In[44]:


import re
from nltk.corpus import stopwords as stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
lemmatizer = WordNetLemmatizer()

def to_lower(str):
    return str.lower()

def remove_url(str):
    return re.sub('http\S+', '', str)

def remove_email(str):
    return re.sub('[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+', '', str)

def remove_punctuation(str):
    return re.sub('[\.!,?:]', '', str)

def remove_stopword(str, language='english'):
    return (" ").join([word for word in str.split() if word not in stopwords.words(language)])

def lemmatize_word(str, language='english'):
    return " ".join([lemmatizer.lemmatize(word) for word in word_tokenize(str)])


def preprocess_text(text_to_process, processing_function_list):
    for process in processing_function_list:
        text_to_process = process(text_to_process)
    return text_to_process

preprocess_functions = [to_lower, remove_url, remove_email, remove_punctuation, remove_stopword]


# In[45]:


print(to_lower("HTLKREMSmedizinTechnik"))

print(remove_url("Follow the link on: https://google.com"))

print(remove_email("The e-mail s.preis@htlkrems.at belongs to me"))

print(remove_punctuation("This are some special marks!: .!? "))

print(remove_stopword("Wie moch i des in Csharp?"))

print(lemmatize_word("There are a lot of chars in this array"))

print(preprocess_text(
    'If Trump Ipsum werent my own words, perhaps Id be dating it. I write the best placeholder text, and I am the biggest developer on the web by far. While thats mock-ups and this is politics, are they really so different?'+ 
    'I think the only card she has is the Lorem card. You could see there was text coming out of her eyes, text coming out of her wherever. Lorem Ipsums father was with Lee Harvey Oswald prior to Oswalds being, you know, shot.',
    preprocess_functions))





# In[46]:


#nltk.download('stopwords')


# In[47]:


import nltk
#nltk.download('tagsets')
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
#nltk.download('averaged_perceptron_tagger')


# ## POS-Tagging mit NLTK

# In[48]:


text = "I love NLP and I will learn NLP in 2 month."
stop_words = set(stopwords.words('english'))
words = [token for token in word_tokenize(text) if token not in stop_words]
print(nltk.pos_tag(words))


# In[49]:


#nltk.help.upenn_tagset()


# ## POS-Tagging mit Spacy

# In[50]:


#SPACEY
import spacy


# In[51]:


nlp = spacy.load("en_core_web_sm")
doc = nlp("I love NLP and I will learn NLP in 2 month.")
words = [token for token in doc if token.is_stop == False]
for token in words:
    print(token.text, token.pos_)


# In[52]:


countyWords = Counter()

for token in doc: 
    countyWords[token.pos_] +=1
    
print(countyWords)


# In[53]:


spacy_words = []
nltk_words = []

with open('dataScientistJobDesc.txt') as f:
    file = f.read()
    tokenized_file = nltk.word_tokenize(file)


# In[54]:


nlp = spacy.load("en_core_web_sm")
doc = nlp(file)

for word in doc:
    if (word.pos_ == 'NOUN' or word.pos_ == 'PROPN'):
        spacy_words.append(word.text)


# In[55]:


for word, pos in nltk.pos_tag(tokenized_file):
    if (pos == 'NN' or pos == 'NNP' or pos == 'NNPS' or pos == 'NNS'):
        nltk_words.append(word)


# ## 3.2.1

# In[57]:


import spacy
from spacy.matcher import Matcher

nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)
doc = nlp(
 "After making the iOS update you won't notice a radical system-wide "
 "redesign: nothing like the aesthetic upheaval we got with iOS 7. Most of "
 "iOS 11's furniture remains the same as in iOS 10. But you will discover "
 "some tweaks once you delve a little deeper."

) 


# In[58]:



print(f"Spacy: {len(spacy_words)}")
print(f"NLTK : {len(nltk_words)}")

from collections import Counter



county = Counter()

for word in doc:
    county[word.pos_] += 1

dict_tag_count = dict(county)
dict_tag_count

county = Counter([token.text for token in doc])
county.most_common(1)


# ## Matchers 

# In[59]:


matcher = Matcher(nlp.vocab) 
pattern = [{"TEXT": "iOS"}, {"IS_DIGIT": True}] 

matcher.add("IOS_VERSION_PATTERN", [pattern])
matches = matcher(doc)
print("Total matches found:", len(matches))

for match_id, start, end in matches:
    print("Match found:", doc[start:end].text)


# In[60]:


doc = nlp(
 "i downloaded Fortnite on my laptop and can't open the game at all. Help? "
 "so when I was downloading Minecraft, I got the Windows version where it "
 "is the '.zip' folder and I used the default program to unpack it... do "
 "I also need to download Winzip?"
) 


# In[61]:


matcher = Matcher(nlp.vocab) 
pattern = [{"LEMMA": "download"}, {"POS": "PROPN"}] 
matcher.add("DOWNLOAD_PATTERN", [pattern])
matches = matcher(doc)
print("Total matches found:", len(matches))

for match_id, start, end in matches:
    print("Match found:", doc[start:end].text)
    


# In[62]:


doc = nlp(
 "Features of the app include a beautiful design, smart search, automatic labels and optional voice responses."
)
pattern = [{"POS": "ADJ"}, {"POS": "NOUN"}, {"POS": "NOUN", "OP": "?"}] 


# In[63]:


doc = nlp(
 "Hello, world! Hello world!"
)
pattern = [{"POS": "INTJ"}, {"POS": "PUNCT", "OP": "?"}, {"POS": "NOUN"}]


# ## 3.3.1 

# In[64]:


matcher = Matcher(nlp.vocab) 
matcher.add("HelloWorld_Pattern", [pattern])
matches = matcher(doc)
print("Total matches found:", len(matches))

for match_id, start, end in matches:
    print(match_id, start, end, doc[start:end].text)


# ## 3.3.2 

# In[65]:


with open("dataScientistJobDesc.txt", encoding="utf-8") as lines:
    document_text = lines.read()
    
nlp = spacy.load("en_core_web_sm")
doc = nlp(document_text)


# In[66]:


matcher = Matcher(nlp.vocab) 
pattern = [{"LEMMA": "experience"}, {"POS": "ADP", "OP": "?"}, {"POS": "ADP", "OP": "?"}, {"POS": "NOUN", "OP": "?"}, {"POS": "NOUN", "OP": "?"}]
matcher.add("DataScientist_Pattern", [pattern])
pattern = [{"POS": "NOUN", "OP": "+"}, {"LEMMA": "experience"}]
matcher.add("DataScientist_Pattern", [pattern])
matches = matcher(doc)
print("Total matches found:", len(matches))

for match_id, start, end in matches:
    print("Match found:", doc[start:end].text)


# # Syntactic Parsing

# ## Example of Spacy Dependency Parser

# In[67]:


import spacy
nlp=spacy.load('en_core_web_sm')

text='I was heading towards North'

print(f"{'Token':{8}} {'dependence':{6}} {'head text':{9}}  {'Dependency explained'} ")
for token in nlp(text):
     print(f"{token.text:{8}} {token.dep_+' =>':{10}}   {token.head.text:{9}}  {spacy.explain(token.dep_)} ")


# ## Dependency Parser Lists in Spacy

# In[68]:


parser_lst = nlp.pipe_labels['parser']

print(len(parser_lst))
print(parser_lst)


# ## Spacy Dependency Tree Example

# In[69]:


from spacy import displacy

displacy.render(doc,jupyter=True)


# ## Iterating over Children
# 

# In[70]:


doc = nlp("I was heading towards North.")
for token in doc:
    print(token.text, token.dep_, token.head.text, [child for child in token.children])


# # NER - Named Entity Recognition
# 

# ## 3.3.1
# 

# In[2]:


import spacy
from spacy import displacy 
nlp = spacy.load('en_core_web_sm')


# In[3]:


doc = nlp('Apple is looking at buying U.K. startup for $1 billion')
for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_, spacy.explain(ent.label_))


# In[4]:


displacy.render(doc, style="ent")


# # 3.3.2

# ### NER-Tags
# GPE: Länder NORP: Nationalitäten oder Religiöse oder Politische Gruppen ORDINAL: Aufzählungen

# ## 3.3.3
# 

# In[5]:


doc = nlp('Dear Joe! I have organized a meeting with Elon Musk from Siemens for tomorrow. Meeting place is Vienna.')
for token in doc:
    print(f'{token.text:10} {token.ent_iob_} {token.ent_type_}')


# # 3.3.4
# 

# In[8]:


nlp = spacy.load('de_core_news_sm')
doc = nlp("Der Ministerrat der Republik Österreich beschloss am 25.März 2014, eine" 
         + "„Unabhängige Untersuchungskommission zur transparenten Aufklärung der Vorkommnisse rund um die Hypo Group Alpe-Adria“ " 
         + "einzusetzen. Die Untersuchungskommission (Manuel Ammann, Carl Baudenbacher, Ernst Wilhelm Contzen, Irmgard Griss, "
         + "Claus-Peter Weber) hat, beginnend mit 1.Mai 2014, durch Auswertung von beigeschafften Unterlagen und allgemein "
         + "zugänglichen Quellen sowie durch Befragung von Auskunftspersonen den maßgeblichen Sachverhalt festgestellt und "
         + "nach fachlichen Kriterien bewertet.")
for token in doc:
    print(f'{token.text:10} {token.ent_iob_} {token.ent_type_}')


# In[25]:



preceeding_PER = ""
new_text = []
for x in range(len(doc)):
if doc[x].ent_type_ == "PER":
    if preceeding_PER != "PER":
        new_text.append("REDACTED")
        
    preceeding_PER = doc[x].ent_type_
else:
    preceeding_PER = ""
    new_text.append(doc[x])
new_text


# In[ ]:





# In[ ]:




