#!/usr/bin/env python
# coding: utf-8

# In[13]:


import re


# In[14]:


#3.1 $ ODER €
p = re.compile("[0-9].[0-9][0-9][$€]")

print(p.findall("Für 5 Semmeln habe ich 3.90€ bezahlt."))
print(p.findall("Für 5 Semmeln habe ich 3.90$ bezahlt."))


# In[15]:


#3.2. Wäre [0-9][0-9][0-9] eine Mögliche Lösung?
# Nicht wirklich. Für alle "Klassen" muss eine beliebige Ziffer zwischen 0 und 9 vorhanden sein.
# Insgesamt wird nur nach einer 3-stelligen Zahl gesucht. Preis kann nicht gefunden werden da das Komma und die Einheit nicht berücksichtigt wird


# In[16]:


#3.3. Neues Pattern:
p = re.compile("[0-9]+[.,][0-9][0-9][$€]?")

print(p.findall("Für 5 Semmeln habe ich 3.90€ bezahlt."))
print(p.findall("Für 15 Semmeln habe ich 19.50$ bezahlt."))
print(p.findall("Für 150 Semmeln habe ich 190,50 bezahlt."))


# In[17]:


#3.4.
#3.4.1. search() vs match()
# Bei der Verwendung von MATCH() muss das erste "Wort" der Zeichenkette mit dem Pattern übereinstimmen. 
# Bei SEARCH() wird  der gesamte String durchsucht
#3.4.2. findall() vs finditer()
# FINDALL gibt alle Übereinstimmungen als Liste von Strings zurück 
# FINDITER einen Iterator von allen


# In[18]:


#3.5. Blog https://www.programiz.com/python-programming/regex durcharbeiten und verstehen


# In[19]:


#3.6. Alle Mailadressen der HTL Krems Homepage ausgeben und in CSV speichern.
import urllib.request as urllib2
from bs4 import BeautifulSoup
import ssl
import csv
import pandas as pd

#3.6.1. Seite in String laden
context = ssl._create_unverified_context()

with urllib2.urlopen('https://www.htlkrems.ac.at/', context=context) as response:
    content = BeautifulSoup(response.read(), 'html.parser').prettify()
    
    #3.6.2. Alle Mailadressen aus String laden
    p = re.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+")
    mailAdresses = pd.Series(p.findall(content))
    
    
    #3.6.3. Mail-Adressen in eine CSV schreiben
    mailAdresses.to_csv('HTLMailAdresses.CSV')


# In[20]:


#3.7. Daten aus FilmDB extrahieren
#3.7.1. Seite in String laden
context = ssl._create_unverified_context()

arr_year = []
arr_title = []
arr_rating = []


with urllib2.urlopen('https://www.imdb.com/chart/top?ref_=nv_mv_250_6:', context=context) as response:
    soup = BeautifulSoup(response.read(), 'html.parser')
    
 #3.7.2. Titelspalten vom Content laden
    titleColumns = soup.findAll('td', attrs = {'class': 'titleColumn'})
    ratingColumns = soup.findAll('td', attrs = {'class': 'ratingColumn imdbRating'})
    
#3.7.3. Daten aus Spalten extrahieren und speichern
    for element in titleColumns:
        arr_year.append(element.find("span", class_="secondaryInfo").text.replace('(', '').replace(')', '').replace('\n', ''))
        arr_title.append(element.find("a").text.replace('\n', ''))

    for element in ratingColumns:
        arr_rating.append(element.find("strong").text.replace('\n', ''))
        
#3.7.4. Arrays in ein Dataframe schreiben
df_movies = pd.DataFrame()

df_movies["year"] = arr_year
df_movies["title"] = arr_title
df_movies["rating"] = arr_rating
    
#3.7.5. Ausgabe DF Dataframe
df_movies.head(3)
# Der Output ist sortiert nach Rating des Eintrags


# In[ ]:





# In[ ]:




