#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm


# ## Datenaufbereitung
# 1. Laden Sie das *spam.csv*-File in das DataFrame *data*
# 2. Ändern Sie die Spaltenbezeichner: v1 -> **target** und v2 -> **message**
# 3. Transfomieren Sie den Inhalt der Spalte *message* in Kleinbuchstaben
# 4. Entfernen Sie Stoppwörter
# 5. Führen Sie Stemming und Lemmatisierung durch

# In[2]:


data = pd.read_csv("spam.csv", encoding="windows-1250", usecols=[0,1])
data.columns = ["target", "message"]
#data


# In[ ]:


import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer


# In[ ]:


def preprocess_text(text_to_process, processing_function_list): 
    text = text_to_process
    for function in processing_function_list:
        text = function(text)
    
    return text

def to_lower(text):
    text = text.lower()
    return text;

def remove_stopword(text):
    stop = stopwords.words('english')
    result = [word for word in text.split() if word not in stop]
    text=""
    for i in result:
        text += i+" "
    return text;

def lemmatize_word(text):
    lemmatizer = WordNetLemmatizer()
    token = word_tokenize(text)
    #token = set(token)
    
    text=""
    for word in token:
         text += (lemmatizer.lemmatize(word))+" "

    return text;

def stemming_word(text):
    pt = PorterStemmer()
    return pt.stem(text)


# In[ ]:


data_edited = data.copy()
preprocess_functions = [to_lower,remove_stopword,lemmatize_word, stemming_word] 

data_edited["message"] = [preprocess_text(x,preprocess_functions) for x in data_edited["message"]]


# In[ ]:


data_edited


# Das Dataset *data* mithilfe von `train_test_split()` in Trainings- und Validierungsdaten aufteilen:

# In[ ]:


dataframe = data.copy()


# In[ ]:


#dataframe = data_edited.copy()


# In[ ]:


train_x, valid_x, train_y, valid_y = train_test_split(dataframe['message'], dataframe['target'], test_size=.3)
print("Check shape: ", train_x.shape, valid_x.shape)


# Die Antworten (*ham* o. *spam*) in 1 oder 0 umwandeln:

# In[ ]:


encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
valid_y = encoder.fit_transform(valid_y)

print("Encoder hat mit 0 und 1 zwei Klassen erkannt: ", encoder.classes_)
print("Die kodierten Antworten der Trainingsdaten: " , train_y)


# Vektorisierung der Daten mit TF-IDF. Mit der Größe von `max_features` kann experimentiert werden. Diese legt den Umfang des Vokabulars fest.

# In[11]:


vectorizer = TfidfVectorizer(analyzer='word', max_features=5000)
vectorizer.fit(dataframe['message'])

print("Vektor mit 5000 Einträgen: ",  vectorizer.get_feature_names)
print("Vokabular: ", vectorizer.get_feature_names())


# In[12]:


train_x_tfidf = vectorizer.transform(train_x)
valid_x_tfidf = vectorizer.transform(valid_x)
print("Check expected shape for train data: ", train_x_tfidf.shape)
print("Check expected shape for validation data: ", valid_x_tfidf.shape)


# Classifier trainieren und validieren:

# In[13]:


clf = naive_bayes.MultinomialNB(alpha=0.2)
# fit the training dataset on the classifier
clf.fit(train_x_tfidf, train_y)

# predict the labels on validation dataset
predictions = clf.predict(valid_x_tfidf)

# retrieve accuracy score
metrics.accuracy_score(predictions, valid_y)


# In[14]:


# ohne Datenbereinigung: 0.9850478468899522


# In[15]:


# mit Datenbereinigung: 0.9868421052631579

