#!/usr/bin/env python
# coding: utf-8

# # UE04 - Convertig Text to Features I

# In[1]:


import spacy
from sklearn.feature_extraction.text import CountVectorizer
from tensorflow.keras.utils import to_categorical


# In[2]:


nlp = spacy.load("en_core_web_sm")
text="Jim loves NLP. He will learn NLP in two months. NLP is future."


# In[3]:


def get_spacy_tokens(text):
    doc = nlp(text)
    return [token.text for token in doc]
text_tokens = get_spacy_tokens(text)


# In[4]:


vectorizer = CountVectorizer(tokenizer=get_spacy_tokens, lowercase=False)

# Erstellung des Vokabulars:
vectorizer.fit(text_tokens)
print("Vocabulary: ", vectorizer.vocabulary_)

# Erstellung der Matrix mit Wortvektoren (One Hote Encoding):
vector = vectorizer.transform(text_tokens)
print("Encoded Document is:")
print(vector.toarray())


# In[5]:


vocab = vectorizer.vocabulary_
#vocab['Jim']

length = 12
to_categorical(vocab['Jim'], length)


# # 4.1 

# In[6]:


def get_spacy_tokens(text):
    doc = nlp(text)
    return [token.text for token in doc]



def one_hot_encoding(sent):
    codiert = []
    for word in get_spacy_tokens(sent):
        codiert.append(to_categorical(vectorizer.vocabulary_[word], len(vectorizer.vocabulary_)))
    return codiert

vocab = one_hot_encoding("Jim loves NLP. He will learn NLP in two months. NLP is future.")

for item in vocab:
    print(item)


# In[ ]:





# In[ ]:




